const path = require('path')
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

const staticPath = './src/main/resources/static/'

module.exports = {
  mode: 'development',
  entry: {
    main: staticPath + 'entry.js'
  },
  output: {
    filename: 'bundle.js',
    path: path.join(__dirname, staticPath, 'dist')
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        use: {
          loader: "babel-loader"
        }
      },
      {
        test: /(\.scss$)|(\.css$)/,
        use: [
          "style-loader",
          MiniCssExtractPlugin.loader,
          "css-loader",
          "sass-loader"
        ]
      }
    ]
  }, 
  plugins: [
    new MiniCssExtractPlugin({
      filename: 'style.css',
    })
  ]
}