import {Calendar} from '@fullcalendar/core';
import timeGridPlugin from '@fullcalendar/timegrid';

$(document).ready(function () {
    if (document.getElementById('calendar')) {
        var calendarEl = document.getElementById('calendar');

        let calendar = new Calendar(calendarEl, {
            plugins: [timeGridPlugin],
            defaultView: 'timeGridWeek',
            allDaySlot: false,
            slotDuration: '00:15:00', // each time row is 30 min
            minTime: '08:00:00', // start from 8.00
            maxTime: '20:00:00', // end in 20.00
            height: 'auto',
            slotLabelFormat: { // format time as HH:MM
                hour: '2-digit',
                minute: '2-digit',
                hour12: false
            },
            firstDay: 1, // show monday first
            
            eventClick: function(info) {
                let dateFormatOptions = {year: 'numeric', month: 'long', day: 'numeric', hour: "2-digit", minute:"2-digit" };
                let startDate = info.event.start.toLocaleDateString("en-US", dateFormatOptions);
                let endDate = info.event.end.toLocaleDateString("en-US", dateFormatOptions);
                let createAppointmentUrl = "http://localhost:8080/appointment/appointment/add";
                let courseId = $("#student-course-nav a.active").attr('data-course-id');

                $('body').append($(`
                <div class="modal" tabindex="-1" role="dialog" id="eventModal">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <form action="${createAppointmentUrl}" method="POST" id="appointmentForm">
                            
                                <div class="modal-header">
                                    <h5 class="modal-title">Make an appointment</h5>
                                </div>
                                <div class="modal-body">
                                    <p>with ${info.event.extendedProps.taName} from ${startDate} to ${endDate}</p>
                                    <input type="hidden" name="eventId" value="${info.event.id}">
                                    <input type="hidden" name="assistantId" value="${info.event.extendedProps.taId}">
                                    <input type="hidden" name="courseId" value="${courseId}">
                                    <div class="form-group">
                                        <label for="appointmentDescription">Description</label>
                                        <input type="text" class="form-control" id="appointmentDescription" name="description" />
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button id="createAppointment" type="submit" class="btn btn-primary">Confirm</button>
                                    <button id="closeModal" type="button" class="btn btn-secondary">Cancel</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                `));

                const eventModal = $('#eventModal');
                eventModal.modal({
                    backdrop: 'static',
                    keyboard: false
                  }) 
                eventModal.modal('show');

                $("#eventModal #closeModal").click(function() {
                    eventModal.modal('hide').data('bs.modal', null);
                    eventModal.remove();
                });

                $('#appointmentForm').submit(function() {
                    event.preventDefault();
                    $("#appointmentForm button").attr("disabled", true)
              
                    var data = $(this).serializeArray().reduce(function(obj, item) {
                      obj[item.name] = item.value
                      return obj
                    }, {});
              
                    $.ajax({
                        url: $(this).attr('action'),
                        type: "POST",
                        contentType: "application/json",
                        data: JSON.stringify(data),
                        success: function(response) {
                            alert('success')
                        },
                        error: function(xhr, status, error) {
                            alert('fail, see console for more info')
                            console.log(JSON.parse(xhr.responseText))
                        },
                        complete: function(response) {
                            $("#appointmentForm button").attr("disabled", false);
                            eventModal.modal('hide').data('bs.modal', null);
                            eventModal.remove();
                        }
                    })
                })

            }
        });

        calendar.render();

        let taId, taIds;
        $("#student-course-nav a").click(function() {
            $("#student-course-nav a").removeClass("active");
            $(this).addClass("active");
            calendar.removeAllEvents();
            taIds = JSON.parse($(this).attr('data-ta-ids'))
            for (let i in taIds) {
                taId = taIds[i];
                fetch(`http://localhost:8080/schedule/schedule/course/calendar?ta_id=${taId}`)
                    .then(res => res.json())
                    .then(result => {
                        for (let i in result) {
                            calendar.addEvent(result[i]);
                        }})
                    .catch(error => {
                        console.log(error);
                    });
            }
        })

    }
});