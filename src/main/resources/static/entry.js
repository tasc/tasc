// javascript partials
require("./js/calendar.js");

// css for calendar
import "../../../../node_modules/@fullcalendar/core/main.css";
import "../../../../node_modules/@fullcalendar/daygrid/main.css";
import "../../../../node_modules/@fullcalendar/timegrid/main.css";

// custom css
import "./css/calendar.scss";
import "./css/overwrite.scss";