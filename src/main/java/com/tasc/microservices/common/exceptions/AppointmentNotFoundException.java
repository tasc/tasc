package com.tasc.microservices.common.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.CONFLICT, reason="Appointment not found for your role")
public class AppointmentNotFoundException extends RuntimeException {}
