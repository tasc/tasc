package com.tasc.microservices.common.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.CONFLICT, reason="Schedule not found")
public class ScheduleNotFoundException extends RuntimeException {}
