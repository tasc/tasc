package com.tasc.microservices.common.proxy;

import com.tasc.microservices.services.schedule.model.Course;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@FeignClient(name = "account-service", url = "localhost:2222")
public interface AccountServiceProxy {

    @GetMapping("/info")
    List<Course> getAccountInfo(@RequestParam("user_id") Long userId);

}
