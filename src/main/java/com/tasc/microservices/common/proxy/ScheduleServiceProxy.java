package com.tasc.microservices.common.proxy;

import com.tasc.microservices.services.accounts.config.ScheduleServiceFallback;
import com.tasc.microservices.services.schedule.model.Course;
import com.tasc.microservices.services.schedule.model.Schedule;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@FeignClient(name = "schedule-service", url = "localhost:3333", fallback = ScheduleServiceFallback.class)
public interface ScheduleServiceProxy {
    @PostMapping("/schedule/create?userId={userId}")
    Schedule createSchedule(@RequestParam("userId") String userId);

    @PostMapping("/schedule/{schedule_id}/addevent")
    Schedule createEvent(@RequestParam("schedule_id") Long schedule_id);

    @GetMapping("/course")
    List<Course> getAllCourses();

    @PostMapping("/course/create")
    Course postCreateCourse(@Valid @RequestBody Course course);

    @PostMapping("/course/{course_id}/addassistant")
    Course postAssignAssistant(
            @PathVariable(value = "course_id") Long courseId,
            @RequestParam("assistant_id") Long assistantId);

    @GetMapping("/course/registeredcourse")
    List<Course> getRegisteredCourses(@RequestParam("user_id") Long userId);
}
