package com.tasc.microservices.services.appointment.model;


import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name= "appointment")
public class Appointment {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    private Long eventId;

    @NotNull
    private Long studentId;

    @NotNull
    private Long assistantId;

    @NotNull
    private Long courseId;

    @NotNull
    private String description;

    public Appointment() {
    }

    public Appointment(Long eventId, Long studentId, Long assistantId, Long courseId, String description) {
        this.eventId = eventId;
        this.studentId = studentId;
        this.assistantId = assistantId;
        this.courseId = courseId;
        this.description = description;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getEventId() {
        return eventId;
    }

    public void setEventId(Long eventId) {
        this.eventId = eventId;
    }

    public Long getStudentId() {
        return studentId;
    }

    public void setStudentId(Long studentId) {
        this.studentId = studentId;
    }

    public Long getAssistantId() {
        return assistantId;
    }

    public void setAssistantId(Long assistantId) {
        this.assistantId = assistantId;
    }

    public Long getCourseId() {
        return courseId;
    }

    public void setCourseId(Long courseId) {
        this.courseId = courseId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
