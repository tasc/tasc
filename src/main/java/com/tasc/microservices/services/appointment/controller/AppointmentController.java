package com.tasc.microservices.services.appointment.controller;

import com.tasc.microservices.common.exceptions.AppointmentNotFoundException;
import com.tasc.microservices.services.accounts.model.Account;
import com.tasc.microservices.services.accounts.model.AccountRepository;
import com.tasc.microservices.services.appointment.model.Appointment;
import com.tasc.microservices.services.appointment.model.AppointmentRepository;
import com.tasc.microservices.services.gateway.config.JwtConfig;
import com.tasc.microservices.services.schedule.model.Course;
import com.tasc.microservices.services.schedule.model.CourseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;

@RestController
public class AppointmentController {

    protected AppointmentRepository appointmentRepository;
    protected AccountRepository accountRepository;
    protected CourseRepository courseRepository;
    protected JwtConfig jwtConfig;
    protected MailerService mailerService;

    @Autowired
    public AppointmentController(
            AppointmentRepository appointmentRepository,
            AccountRepository accountRepository,
            CourseRepository courseRepository,
            JwtConfig jwtConfig,
            MailerService mailerService) {
        this.appointmentRepository = appointmentRepository;
        this.accountRepository = accountRepository;
        this.courseRepository = courseRepository;
        this.jwtConfig = jwtConfig;
        this.mailerService = mailerService;
    }

    @GetMapping(value = "/appointment", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Appointment> getAppointments(
            @CookieValue("Authorization") String jwtToken, @RequestParam("courseId") Long courseId) {
        String role = jwtConfig.getRole(jwtToken);
        Long accountId = jwtConfig.getId(jwtToken);
        switch (role) {
            case "STUDENT":
                return appointmentRepository.getAppointmentsByCourseIdAndStudentId(courseId, accountId);
            case "TA":
                return appointmentRepository.getAppointmentsByCourseIdAndAssistantId(courseId, accountId);
            default:
                throw new AppointmentNotFoundException();
        }
    }

    @PostMapping("/appointment/add")
    public Appointment postMakeAppointment(
            @CookieValue("Authorization") String jwtToken,
            @Valid @RequestBody Map<String, String> body) {
        Long studentId = jwtConfig.getId(jwtToken);
        Appointment appointment = new Appointment(
                Long.parseLong(body.get("eventId")),
                studentId,
                Long.parseLong(body.get("assistantId")),
                Long.parseLong(body.get("courseId")),
                body.get("description"));
        Account student = accountRepository.getOne(studentId);
        Account assistant = accountRepository.getOne(Long.parseLong(body.get("assistantId")));
        Course course = courseRepository.getOne(Long.parseLong(body.get("courseId")));

        String content = String.format("Hi %s!\n", student.getName());
        content += String.format(
                "You have an upcoming appointment with %s from %s.\n",
                assistant.getName(), course.getCourseName());
        mailerService.sendEmail(student.getEmail(), "New Appointment", content);

        content = String.format("Hi %s!\n", assistant.getName());
        content += String.format(
                "You have an upcoming appointment with %s from %s.\n",
                student.getName(), course.getCourseName());
        mailerService.sendEmail(student.getEmail(), "New Appointment", content);

        return appointmentRepository.save(appointment);
    }
}
