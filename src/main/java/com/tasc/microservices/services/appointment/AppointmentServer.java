package com.tasc.microservices.services.appointment;

import com.tasc.microservices.common.annotations.Generated;
import com.tasc.microservices.common.config.DBConfiguration;
import com.tasc.microservices.services.appointment.config.AppointmentConfiguration;
import com.tasc.microservices.services.appointment.model.AppointmentRepository;
import com.tasc.microservices.services.appointment.config.WebSecurityConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Import;

@Generated
@SpringBootApplication
@EnableDiscoveryClient
@Import({AppointmentConfiguration.class, WebSecurityConfig.class, DBConfiguration.class})
public class AppointmentServer {

    @Autowired
    AppointmentRepository appointmentRepository;

    public static void main(String[] args) {
        System.out.println("Appointment Server running!");
        System.setProperty("spring.config.name", "appointment-server");
        SpringApplication.run(AppointmentServer.class, args);
    }
}
