package com.tasc.microservices.services.appointment.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Service;

@Service
public class MailerService {

    @Autowired
    JavaMailSender mailSender;

    public void sendEmail(String to, String subject, String content) {
        MimeMessagePreparator mailMessage = mimeMessage -> {
            MimeMessageHelper message = new MimeMessageHelper(
                    mimeMessage, true, "UTF-8");
            message.setFrom("tasc.noreply@gmail.com", "Teaching Assistant Student Connector");
            message.setTo(to);
            message.setSubject(subject);
            message.setText(content);
        };
        mailSender.send(mailMessage);
    }
}
