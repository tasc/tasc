package com.tasc.microservices.services.appointment.config;

import com.tasc.microservices.common.annotations.Generated;
import com.tasc.microservices.services.gateway.config.JwtConfig;
import org.springframework.context.annotation.Bean;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Generated
@EnableWebSecurity
@Order(500)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().and().cors().disable();
    }

    @Bean
    public JwtConfig jwtConfig() {
        return new JwtConfig();
    }
}