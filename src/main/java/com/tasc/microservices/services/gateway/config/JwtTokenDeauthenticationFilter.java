package com.tasc.microservices.services.gateway.config;

import com.tasc.microservices.common.annotations.Generated;
import org.springframework.web.filter.OncePerRequestFilter;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.servlet.*;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URI;

@Generated
public class JwtTokenDeauthenticationFilter extends OncePerRequestFilter {

    private JwtConfig jwtConfig;

    public JwtTokenDeauthenticationFilter(JwtConfig jwtConfig) {
        this.jwtConfig = jwtConfig;
    }

    @Override
    public void doFilterInternal(
            HttpServletRequest request,
            HttpServletResponse response,
            FilterChain chain) throws IOException, ServletException {

        if (request.getRequestURL().toString().matches(".*/deauth.*")) {
            for (Cookie cookie : request.getCookies()) {
                cookie.setMaxAge(0);
                response.addCookie(cookie);
            }
            URI baseUri = ServletUriComponentsBuilder.fromCurrentRequest().build().toUri();
            String redirectUrl = "http://" + baseUri.getHost() + (baseUri.getPort() < 0 ? "" : ":" + baseUri.getPort());
            response.sendRedirect(redirectUrl + "/");
        } else {
            chain.doFilter(request, response);
        }
    }
}
