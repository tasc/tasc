package com.tasc.microservices.services.gateway.config;

import com.tasc.microservices.common.annotations.Generated;
import com.tasc.microservices.services.accounts.model.Account;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.sql.Date;
import java.util.stream.Collectors;

@Generated
public class JwtConfig {
    @Value("${security.jwt.uri:/auth/**}")
    private String uri;

    @Value("${security.jwt.header:Authorization}")
    private String header;

    @Value("${security.jwt.prefix:Bearer }")
    private String prefix;

    @Value("${security.jwt.expiration:#{24*60*60}}")
    private int expiration;

    @Value("${security.jwt.secret:JwtSecretKey}")
    private String secret;

    public String getUri() {
        return uri;
    }

    public String getHeader() {
        return header;
    }

    public String getPrefix() {
        return prefix;
    }

    public int getExpiration() {
        return expiration;
    }

    public String getSecret() {
        return secret;
    }

    public Cookie getJwtCookie(HttpServletRequest request) {
        Cookie target = null;
        if (request.getCookies() != null) {
            for (Cookie cookie : request.getCookies()) {
                if (cookie.getName().equals(getHeader())) {
                    target = cookie;
                    System.out.println("TOKEN: " + cookie.getValue());
                    break;
                }
            }
        }
        return target;
    }

    public String getRole(String token) {
        if (token == null) return "";
        Claims claims = Jwts.parser()
                .setSigningKey(getSecret().getBytes())
                .parseClaimsJws(token)
                .getBody();
        return claims.get("authorities").toString()
                .replace("[ROLE_", "")
                .replace("]", "");
    }

    public String getRole(HttpServletRequest request) {
        Cookie jwtCookie = getJwtCookie(request);
        if (jwtCookie != null) {
            return getRole(jwtCookie.getValue());
        }
        return "";
    }

    public Long getId(String token) {
        if (token == null) return (long) 0;
        Claims claims = Jwts.parser()
                .setSigningKey(getSecret().getBytes())
                .parseClaimsJws(token)
                .getBody();
        return  new Long((Integer)claims.get("userId")); // fix Integer to Long conversion
    }

    public Long getId(HttpServletRequest request) {
        Cookie jwtCookie = getJwtCookie(request);
        if (jwtCookie != null) {
            return getId(jwtCookie.getValue());
        }
        return (long) 0;
    }

    public String createToken(Account account) {
        Long now = System.currentTimeMillis();
        String token = Jwts.builder()
                .setSubject(account.getName())
                .claim("authorities", AuthorityUtils
                        .commaSeparatedStringToAuthorityList("ROLE_" + account.getRole())
                        .stream().map(GrantedAuthority::getAuthority).collect(Collectors.toList()))
                .claim("userId", account.getId())
                .setIssuedAt(new Date(now))
                .setExpiration(new Date(now + getExpiration() * 1000))  // in milliseconds
                .signWith(SignatureAlgorithm.HS512, getSecret().getBytes())
                .compact();
        return token;
    }
}