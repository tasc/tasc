package com.tasc.microservices.services.gateway.config;

import javax.servlet.http.HttpServletResponse;

import com.tasc.microservices.common.annotations.Generated;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Generated
@EnableWebSecurity
@Order(1)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
    @Autowired
    private JwtConfig jwtConfig;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .csrf().disable()

                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()

                .exceptionHandling()
                .authenticationEntryPoint(
                        (req, rsp, e) -> rsp.sendError(HttpServletResponse.SC_UNAUTHORIZED)
                )
                .and()

                .addFilterAfter(
                        new JwtTokenDeauthenticationFilter(jwtConfig),
                        UsernamePasswordAuthenticationFilter.class
                )

                .addFilterAfter(
                        new JwtTokenAuthenticationFilter(jwtConfig),
                        JwtTokenDeauthenticationFilter.class
                )

                .authorizeRequests()
                .antMatchers("/").permitAll()

                .antMatchers("/login").permitAll()

                .antMatchers("/register").permitAll()

                .antMatchers("/deauth").permitAll()

                .antMatchers("/accounts/**").permitAll()

                .antMatchers("/schedule/course").hasAnyRole("ADMIN", "STUDENT", "TA")

                .antMatchers("/schedule/course/create/**").hasRole("ADMIN")

                .antMatchers("/schedule/schedule").permitAll()

                .antMatchers("/schedule/schedule/create/**").permitAll()

                .antMatchers("/appointment/**").permitAll()

                .anyRequest().authenticated();

//            .and()
//                .formLogin()
//                .loginPage("/login")
//                .loginProcessingUrl("/accounts/auth")
//                .defaultSuccessUrl("/tahome", true)
//                .failureUrl("/login.html?error=true")
//                .failureHandler(authenticationFailureHandler());
    }

    @Bean
    public JwtConfig jwtConfig() {
        return new JwtConfig();
    }
}