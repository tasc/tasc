package com.tasc.microservices.services.gateway;

import com.tasc.microservices.common.annotations.Generated;
import com.tasc.microservices.services.gateway.config.WebSecurityConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.context.annotation.Import;

@Generated
@SpringBootApplication
@EnableDiscoveryClient
@EnableZuulProxy
@Import(WebSecurityConfig.class)
public class GatewayServer {
    public static void main(String[] args) {
        System.out.println("Gateway Server running!");
        System.setProperty("spring.config.name", "gateway-server");
        SpringApplication.run(GatewayServer.class, args);
    }
}
