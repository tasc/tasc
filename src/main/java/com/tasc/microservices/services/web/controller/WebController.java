package com.tasc.microservices.services.web.controller;

import com.tasc.microservices.common.proxy.AccountServiceProxy;
import com.tasc.microservices.common.proxy.ScheduleServiceProxy;
import com.tasc.microservices.services.gateway.config.JwtConfig;
import com.tasc.microservices.services.schedule.model.Course;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Controller
public class WebController {

    @Autowired
    protected ScheduleServiceProxy scheduleServiceProxy;

    @Autowired
    protected AccountServiceProxy accountServiceProxy;

    @Autowired
    private JwtConfig jwtConfig;

    @GetMapping("/")
    public String getIndex(HttpServletRequest request, HttpServletResponse response, Model model) {
        String role = jwtConfig.getRole(request);
        switch (role) {
            case "ADMIN":
                return "adminhome";
            case "TA":
                return "tahome";
            case "STUDENT":
                Long userId = jwtConfig.getId(request);
                List<Course> courses =  scheduleServiceProxy.getRegisteredCourses(userId);
                model.addAttribute("courses", courses);
                return "studenthome";
            default:
                return "index";
        }
    }

    @GetMapping("/login")
    public String getLogIn() {
        return "login";
    }

    @GetMapping("/register")
    public String getRegister() {
        return "register";
    }

    @GetMapping("/profile")
    public String getProfile(HttpServletRequest request, ModelMap model) {
        model.put("accoutnInfo", accountServiceProxy.getAccountInfo(jwtConfig.getId(request)));
        return "taprofile";
    }

    @GetMapping("/calendar")
    public String displaySchedule() {
        return "calendar";
    }
}
