package com.tasc.microservices.services.web;

import com.tasc.microservices.common.annotations.Generated;
import com.tasc.microservices.common.config.DBConfiguration;
import com.tasc.microservices.services.web.config.WebConfiguration;
import com.tasc.microservices.services.web.config.WebSecurityConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Import;

@Generated
@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients("com.tasc.microservices.common.proxy")
@Import({WebSecurityConfig.class, WebConfiguration.class})
public class WebServer {
    public static void main(String[] args) {
        System.out.println("Web Server running!");
        System.setProperty("spring.config.name", "web-server");
        SpringApplication.run(WebServer.class, args);
    }
}
