package com.tasc.microservices.services.web.config;

import com.tasc.microservices.common.annotations.Generated;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Generated
@ComponentScan
@Configuration
public class WebConfiguration {

}
