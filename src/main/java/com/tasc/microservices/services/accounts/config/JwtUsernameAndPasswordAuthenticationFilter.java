package com.tasc.microservices.services.accounts.config;

import com.tasc.microservices.common.annotations.Generated;
import com.tasc.microservices.services.accounts.model.Account;
import com.tasc.microservices.services.accounts.model.AccountRepository;
import com.tasc.microservices.services.gateway.config.JwtConfig;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import java.io.IOException;
import java.net.URI;
import java.sql.Date;
import java.util.Collections;
import java.util.Map;

import javax.servlet.FilterChain;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

@Generated
public class JwtUsernameAndPasswordAuthenticationFilter
        extends UsernamePasswordAuthenticationFilter {

    private UserDetailsServiceImpl userDetailsService;

    private AuthenticationManager authManager;

    private final JwtConfig jwtConfig;

    public JwtUsernameAndPasswordAuthenticationFilter(
            AuthenticationManager authManager, JwtConfig jwtConfig, UserDetailsServiceImpl userDetailsService) {
        this.authManager = authManager;
        this.jwtConfig = jwtConfig;
        this.userDetailsService = userDetailsService;

        // By default, UsernamePasswordAuthenticationFilter listens to "/login" path.
        // In our case, we use "/auth". So, we need to override the defaults.
        this.setRequiresAuthenticationRequestMatcher(
                new AntPathRequestMatcher(jwtConfig.getUri(), "POST")
        );
    }

    @Override
    public Authentication attemptAuthentication(
            HttpServletRequest request, HttpServletResponse response)
            throws AuthenticationException {

        // 1. Get credentials from request
//            AccountCredentials creds = new ObjectMapper()
//                    .readValue(request.getParameterMap(), AccountCredentials.class);
        Map<String, String[]> creds = request.getParameterMap();

        // 2. Create auth object (contains credentials) which will be used by auth manager
        UsernamePasswordAuthenticationToken authToken = new UsernamePasswordAuthenticationToken(
                creds.get("username")[0], creds.get("password")[0], Collections.emptyList());

        // 3. Authentication manager authenticate the user,
        // and use UserDetialsServiceImpl::loadUserByUsername() method to load the user.
        return authManager.authenticate(authToken);
    }

    // Upon successful authentication, generate a token.
    // The 'auth' passed to successfulAuthentication() is the current authenticated user.
    @Override
    protected void successfulAuthentication(
            HttpServletRequest request, HttpServletResponse response,
            FilterChain chain, Authentication auth)
            throws IOException {
        Account account = userDetailsService.getByUsername(auth.getName());
        String token = jwtConfig.createToken(account);
        response.addHeader(jwtConfig.getHeader(), jwtConfig.getPrefix() + token);
        Cookie cookie = new Cookie(jwtConfig.getHeader(), token);
        cookie.setPath("/");
        cookie.setMaxAge(24 * 60 * 60);
        response.addCookie(cookie);
        URI baseUri = ServletUriComponentsBuilder.fromCurrentRequest().build().toUri();
        String redirectUrl = "http://" + baseUri.getHost() + (baseUri.getPort() < 0 ? "" : ":" + baseUri.getPort());
        response.sendRedirect(redirectUrl + "/");
    }

    @Override
    protected void unsuccessfulAuthentication(
            HttpServletRequest request,
            HttpServletResponse response,
            AuthenticationException failed) throws IOException {
        URI baseUri = ServletUriComponentsBuilder.fromCurrentRequest().build().toUri();
        String redirectUrl = "http://" + baseUri.getHost() + (baseUri.getPort() < 0 ? "" : ":" + baseUri.getPort());
        System.out.println(redirectUrl);
        response.sendRedirect(redirectUrl + "/login?error=true");
    }
}
