package com.tasc.microservices.services.accounts.controller;

import com.tasc.microservices.common.exceptions.AccountNotFoundException;
import com.tasc.microservices.common.proxy.ScheduleServiceProxy;
import com.tasc.microservices.services.accounts.model.Account;
import com.tasc.microservices.services.accounts.model.AccountRepository;

import java.io.IOException;
import java.net.URI;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.http.HttpResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.web.servlet.view.RedirectView;

@RestController
public class AccountsController {

    protected AccountRepository accountRepository;
    protected BCryptPasswordEncoder bCryptPasswordEncoder;
    protected ScheduleServiceProxy scheduleServiceProxy;

    @Autowired
    public AccountsController(
            AccountRepository accountRepository,
            BCryptPasswordEncoder bCryptPasswordEncoder,
            ScheduleServiceProxy scheduleServiceProxy) {
        this.accountRepository = accountRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
        this.scheduleServiceProxy = scheduleServiceProxy;
    }

    @GetMapping(path = "/info")
    public Account getAccountInfo(@RequestParam(value = "user_id") Long userId) {
        return this.accountRepository.findOneById(userId);
    }

    @GetMapping(value = "/accounts", produces = MediaType.APPLICATION_JSON_VALUE)
    public Page<Account> getAccounts(Pageable pageable) {
        return accountRepository.findAll(pageable);
    }

    @PostMapping(path = "/register", consumes = "application/x-www-form-urlencoded")
    public void postCreateAccount(HttpServletResponse response, Account account) throws IOException {
        account.setPassword(bCryptPasswordEncoder.encode(account.getPassword()));
        accountRepository.save(account);
        scheduleServiceProxy.createSchedule(String.valueOf(account.getId()));
        URI baseUri = ServletUriComponentsBuilder.fromCurrentRequest().build().toUri();
        String redirectUrl = "http://" + baseUri.getHost() + (baseUri.getPort() < 0 ? "" : ":" + baseUri.getPort());
        response.sendRedirect(redirectUrl + "/");
    }

    @PostMapping("/accounts/{account_id}/enrol")
    public Account postEnrolCourse(
            @PathVariable(value = "account_id") Long accountId,
            @RequestParam(value = "course_id") Long courseId) {
        return accountRepository.findById(accountId).map(account -> {
            account.addCourse(courseId);
            return accountRepository.save(account);
        }).orElseThrow(AccountNotFoundException::new);
    }
}
