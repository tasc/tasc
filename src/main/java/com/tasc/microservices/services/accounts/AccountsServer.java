package com.tasc.microservices.services.accounts;

import com.tasc.microservices.common.annotations.Generated;
import com.tasc.microservices.common.config.DBConfiguration;
import com.tasc.microservices.services.accounts.config.AccountsConfiguration;
import com.tasc.microservices.services.accounts.config.WebSecurityConfig;
import com.tasc.microservices.services.accounts.model.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Import;

@Generated
@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients("com.tasc.microservices.common.proxy")
@Import({AccountsConfiguration.class, WebSecurityConfig.class, DBConfiguration.class})
public class AccountsServer {
    @Autowired
    protected AccountRepository accountRepository;

    public static void main(String[] args) {
        System.out.println("Account Server running!");
        System.setProperty("spring.config.name", "accounts-server");
        SpringApplication.run(AccountsServer.class, args);
    }
}
