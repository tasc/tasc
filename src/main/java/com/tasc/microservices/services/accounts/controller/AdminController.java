package com.tasc.microservices.services.accounts.controller;

import com.tasc.microservices.common.exceptions.AccountNotFoundException;
import com.tasc.microservices.common.exceptions.ScheduleNotFoundException;
import com.tasc.microservices.common.proxy.ScheduleServiceProxy;
import com.tasc.microservices.services.accounts.model.Account;
import com.tasc.microservices.services.accounts.model.AccountRepository;
import com.tasc.microservices.services.schedule.model.Course;
import com.tasc.microservices.services.schedule.model.CourseRepository;
import com.tasc.microservices.services.schedule.model.Schedule;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.net.URI;
import java.util.List;
import java.util.Optional;

@RestController
public class AdminController {

    protected AccountRepository accountRepository;
    protected ScheduleServiceProxy scheduleServiceProxy;

    @Autowired
    public AdminController(
            AccountRepository accountRepository,
            ScheduleServiceProxy scheduleServiceProxy) {
        this.accountRepository = accountRepository;
        this.scheduleServiceProxy = scheduleServiceProxy;
    }


    @PostMapping("/admin/courses/create")
    public Course postCreateCourse(
            @Valid @RequestBody Course course) {
        return scheduleServiceProxy.postCreateCourse(course);
    }

    @PostMapping(name = "/admin/course/addassistant", consumes = "application/x-www-form-urlencoded")
    public void postAssignAssistant(HttpServletResponse response,
                                    AssistantAssignInfo data) throws IOException {
        scheduleServiceProxy.postAssignAssistant(data.getCourse_id(), data.getAssistant_id());
        URI baseUri = ServletUriComponentsBuilder.fromCurrentRequest().build().toUri();
        String redirectUrl = "http://" + baseUri.getHost() + (baseUri.getPort() < 0 ? "" : ":" + baseUri.getPort());
        response.sendRedirect(redirectUrl + "/");
    }

    @PostMapping("/admin/setRole")
    public Account setRole(
            @RequestParam("account_id") Long accountId,
            @RequestParam("role") String role) {
//        Account tempAccount = accountRepository.getOne(accountId);
//        tempAccount.setRole(role);
//        return accountRepository.save(account);
        return accountRepository.findById(accountId).map(account -> {
            account.setRole(role);
            return accountRepository.save(account);
        }).orElseThrow(AccountNotFoundException::new);
    }
}


class AssistantAssignInfo {
    private Long course_id;
    private Long assistant_id;

    public Long getCourse_id() {
        return course_id;
    }

    public void setCourse_id(Long course_id) {
        this.course_id = course_id;
    }

    public Long getAssistant_id() {
        return assistant_id;
    }

    public void setAssistant_id(Long assistant_id) {
        this.assistant_id = assistant_id;
    }
}
