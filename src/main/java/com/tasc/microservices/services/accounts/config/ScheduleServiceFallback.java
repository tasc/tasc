package com.tasc.microservices.services.accounts.config;

import com.tasc.microservices.common.proxy.ScheduleServiceProxy;
import com.tasc.microservices.services.schedule.model.Course;
import com.tasc.microservices.services.schedule.model.Schedule;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.List;

@Component
public class ScheduleServiceFallback implements ScheduleServiceProxy {

    @Override
    public Schedule createSchedule(String userId) { return new Schedule(); }

    @Override
    public Schedule createEvent(Long schedule_id) { return new Schedule(); }

    @Override
    public List<Course> getAllCourses() { return new ArrayList<>(); }

    @Override
    public Course postCreateCourse(Course course) { return new Course(); }

    @Override
    public Course postAssignAssistant(Long courseId, Long assistantId) { return new Course(); }

    @Override
    public List<Course> getRegisteredCourses(@RequestParam("user_id") Long userId) {
        ArrayList<Course> courses = new ArrayList<>();
        courses.add(new Course());
        return courses;
    }
}

