package com.tasc.microservices.services.schedule;

import com.tasc.microservices.common.annotations.Generated;
import com.tasc.microservices.common.config.DBConfiguration;
//import com.tasc.microservices.services.schedule.config.ScheduleConfiguration;
import com.tasc.microservices.services.schedule.config.WebSecurityConfig;
import com.tasc.microservices.services.schedule.model.CourseRepository;

import com.tasc.microservices.services.schedule.model.Event;
import com.tasc.microservices.services.schedule.model.EventRepository;
import com.tasc.microservices.services.schedule.model.ScheduleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Import;

@Generated
@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients("com.tasc.microservices.common.proxy")
@Import({WebSecurityConfig.class, DBConfiguration.class})
public class ScheduleServer {

    @Autowired
    protected CourseRepository courseRepository;

    @Autowired
    protected EventRepository eventRepository;

    @Autowired
    protected ScheduleRepository scheduleRepository;

    public static void main(String[] args) {
        System.out.println("Schedule Server running!");
        System.setProperty("spring.config.name", "schedule-server");
        SpringApplication.run(ScheduleServer.class, args);
    }
}
