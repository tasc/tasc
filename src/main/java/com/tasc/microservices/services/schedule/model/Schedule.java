package com.tasc.microservices.services.schedule.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name= "schedule")
public class Schedule {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    private Long userId;

    @OneToMany
    private List<Event> timeSlots;

    public Schedule() {
        this.timeSlots = new ArrayList<>();
    }

    public Schedule(List<Event> timeSlots) {
        this.timeSlots = timeSlots;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public List<Event> getTimeSlots() {
        return timeSlots;
    }

    public void setTimeSlots(List<Event> timeSlots) {
        this.timeSlots = timeSlots;
    }

    public void addTimeSlot(Event event) {
        this.timeSlots.add(event);
    }
}
