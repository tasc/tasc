package com.tasc.microservices.services.schedule.model;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import com.tasc.microservices.services.schedule.model.Schedule;
import com.tasc.microservices.services.schedule.model.Event;
import java.io.IOException;
import java.util.Calendar;
import java.util.Formatter;

public class CalendarScheduleSerializer extends StdSerializer<Schedule> {
    /**
     * serializes schedule objecs to match fullcalendar's event sources interface,
     * https://fullcalendar.io/
     */
    private String taName;

    public CalendarScheduleSerializer(String taName) {
        this(null, taName);
    }

    public CalendarScheduleSerializer(Class<Schedule> t, String taName) {
        super(t);
        this.taName = taName;
    }

    private String timeBlockToHourMinuteSecond(Byte timeBlock) {
        Formatter fmt = new Formatter();
        Calendar cal = Calendar.getInstance();
        // set starting time to 08:00:00
        cal.set(Calendar.HOUR_OF_DAY, 8);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.add(Calendar.MINUTE, 15*timeBlock.intValue());
        fmt = new Formatter();
        fmt.format("%tH:%tM:%tS", cal, cal, cal);
        return fmt.toString();
    }

    private void writeEvent(JsonGenerator jgen, Event event, Long taId, Long scheduleId) throws IOException {
        jgen.writeStartObject();
        jgen.writeNumberField("id", event.getId());
        jgen.writeNumberField("taId", taId);
        jgen.writeStringField("taName", this.taName);
        jgen.writeNumberField("scheduleId", scheduleId);
        jgen.writeStringField("title", this.taName);
        jgen.writeStringField("start", String.format("%sT%s",
                event.getStartDate().toString(), timeBlockToHourMinuteSecond(event.getTimeBlock())));
        // one time block is 15 minutes long
        jgen.writeStringField("end", String.format("%sT%s",
                event.getStartDate().toString(), timeBlockToHourMinuteSecond( (byte) (event.getTimeBlock()+1) )));
        jgen.writeEndObject();
    }

    @Override
    public void serialize(
            Schedule value, JsonGenerator jgen, SerializerProvider provider)
            throws IOException, JsonProcessingException {

        jgen.writeStartArray();
        for(Event event : value.getTimeSlots()) {
            if (event.getStartDate() != null) { // single event
                writeEvent(jgen, event, value.getUserId(), value.getId());
            } else { // repeating event
                for(Event childEvent : event.getChildEvents()) {
                    writeEvent(jgen, childEvent, value.getUserId(), value.getId());
                }
            }
        }
        jgen.writeEndArray();
    }
}