package com.tasc.microservices.services.schedule.model;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ScheduleRepository extends JpaRepository<Schedule, Long> {

    Schedule findOneByUserId(Long userId);

}
