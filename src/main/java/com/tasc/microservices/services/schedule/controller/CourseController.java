package com.tasc.microservices.services.schedule.controller;

import com.tasc.microservices.common.exceptions.AccountNotFoundException;
import com.tasc.microservices.common.exceptions.CourseNotFoundException;
import com.tasc.microservices.common.exceptions.ScheduleNotFoundException;
import com.tasc.microservices.services.accounts.model.Account;
import com.tasc.microservices.services.accounts.model.AccountRepository;
import com.tasc.microservices.services.gateway.config.JwtConfig;
import com.tasc.microservices.services.schedule.model.Course;
import com.tasc.microservices.services.schedule.model.CourseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Optional;
import java.util.List;

@RestController
public class CourseController {

    @Autowired
    private JwtConfig jwtConfig;

    protected CourseRepository courseRepository;
    protected AccountRepository accountRepository;

    @Autowired
    public CourseController(CourseRepository courseRepository, AccountRepository accountRepository) {
        this.courseRepository = courseRepository;
        this.accountRepository = accountRepository;
    }

    @GetMapping(value = "/course", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Course> getAllCourse() {
        return courseRepository.findAll();
    }

    @GetMapping(value = "/course/registeredcourse", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Course> getRegisteredCourseByUser(@RequestParam("user_id") Long userId) {
        Optional<Account> userAccount = accountRepository.findById(userId);
        userAccount.orElseThrow(AccountNotFoundException::new);
        return courseRepository.findAllById(userAccount.get().getCourse());
    }

    @PostMapping(value = "/course/create", produces = MediaType.APPLICATION_JSON_VALUE)
    public Course postCreateCourse(@Valid @RequestBody Course course) {
        return courseRepository.save(course);
    }

    @PostMapping(value = "/course/{course_id}/addassistant", produces = MediaType.APPLICATION_JSON_VALUE)
    public Course postAssignAssistant(
            @PathVariable(value = "course_id") Long courseId,
            @RequestParam("assistant_id") Long assistantId) {
        return courseRepository.findById(courseId).map(course -> {
            course.addTeachingAssistants(assistantId);
            return courseRepository.save(course);
        }).orElseThrow(CourseNotFoundException::new);
    }

}
