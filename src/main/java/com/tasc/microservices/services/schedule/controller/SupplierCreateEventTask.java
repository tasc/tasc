package com.tasc.microservices.services.schedule.controller;

import com.tasc.microservices.services.schedule.model.Event;
import com.tasc.microservices.services.schedule.model.SingleEvent;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.function.Supplier;

public class SupplierCreateEventTask implements Supplier<Event> {

    private String method;
    private int iteration;
    private byte timeBlock;
    private String startDate;

    public SupplierCreateEventTask(
            String method, int iteration, byte timeBlock, String startDate)
            throws InterruptedException {
        this.method = method;
        this.iteration = iteration;
        this.timeBlock = timeBlock;
        this.startDate = startDate;
    }

    @Override
    public Event get() {
        Calendar calendarInstance = Calendar.getInstance();
        try {
            calendarInstance.setTime((new SimpleDateFormat("dd/MM/yyyy")).parse(this.startDate));
        } catch (ParseException e) {
            System.out.println("event creation failed, parse error");
            return null;
        }

        if (this.method.equalsIgnoreCase("daily")) {
            calendarInstance.add(Calendar.DAY_OF_MONTH, iteration);
        } else if (this.method.equalsIgnoreCase("weekly")) {
            calendarInstance.add(Calendar.WEEK_OF_MONTH, iteration);
        } else if (this.method.equalsIgnoreCase("monthly")) {
            calendarInstance.add(Calendar.MONTH, iteration);
        }

        Event event = new SingleEvent();
        event.setStartDate(new Date(calendarInstance.getTimeInMillis()));
        event.setTimeBlock(this.timeBlock);
        return event;
    }
}

//public class SupplierFactorialTask implements Supplier<Integer> {
//
//    private int number;
//
//    public SupplierFactorialTask(int number) throws InterruptedException {
//        this.number = number;
//    }
//
//    @Override
//    public Integer get(){
//        int fact = 1;
//
//        for (int count = number; count > 1 ; count--) {
//            fact = fact * count;
//            try {
//                Thread.sleep(1000);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//        }
//
//        return fact;
//    }
//}