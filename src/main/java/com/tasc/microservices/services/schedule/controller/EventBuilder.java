package com.tasc.microservices.services.schedule.controller;

import com.tasc.microservices.services.schedule.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Calendar;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

@Service
public class EventBuilder {

    private Event event;
    private Map<String, String> params;

    private EventRepository eventRepository;

    @Autowired
    EventBuilder(EventRepository eventRepository) {
        this.eventRepository = eventRepository;
    }

    void setParams(Map<String, String> params) {
        this.params = params;
    }

    // old build method
//    void build() throws ParseException {
//        this.event = new SingleEvent();
//        if (this.params.get("repeating").equalsIgnoreCase("true")) {
//            event = new RepeatingEvent();
//            List<Event> childEvents = new ArrayList<>();
//
//            for (int i = 0; i < Integer.parseInt(this.params.get("times")); i++) {
//                Event child = buildEvent(this.params.get("method"), i);
//                childEvents.add(child);
//                eventRepository.save(child);
//            }
//            event.setChildEvents(childEvents);
//        } else {
//            event = buildEvent();
//        }
//    }

    void build() throws ParseException, InterruptedException, ExecutionException {
        this.event = new SingleEvent();
        if (this.params.get("repeating").equalsIgnoreCase("true")) {
            ExecutorService executorService = Executors.newFixedThreadPool(5);

            event = new RepeatingEvent();
            List<Event> childEvents = new ArrayList<>();

            List<CompletableFuture<Event>> completableFuturesList = new ArrayList<>();
            for (int i = 0; i < Integer.parseInt(this.params.get("times")); i++) {
                CompletableFuture<Event> completableFuture = CompletableFuture.supplyAsync(new SupplierCreateEventTask(
                        this.params.get("method"), i, Byte.parseByte(this.params.get("time_block")),
                        this.params.get("start_date")), executorService)
                        .thenCompose(child -> CompletableFuture.supplyAsync(() -> {
                            childEvents.add(child);
                            eventRepository.save(child);
                            return child;
                        }));
                completableFuturesList.add(completableFuture);
            }
            CompletableFuture<Void> combinedFutures = CompletableFuture.allOf(
                    completableFuturesList.toArray(new CompletableFuture[completableFuturesList.size()]));
            combinedFutures.get();

            CompletableFuture<List<Event>> allEventsFuture = combinedFutures.thenApply(v -> {
                return completableFuturesList.stream()
                        .map(future -> future.join())
                        .collect(Collectors.toList());
            });

            event.setChildEvents(allEventsFuture.get());
        } else {
            event = buildEvent();
        }
    }

    private Event buildEvent() throws ParseException {
        Event event = new SingleEvent();
        event.setStartDate(new Date((new SimpleDateFormat("dd/MM/yyyy")).parse(this.params.get("start_date")).getTime()));
        event.setTimeBlock(Byte.parseByte(this.params.get("time_block")));
        return event;
    }

    // no longer used since use async task SupplierCreateEventTask for repeated events
    private Event buildEvent(String method, int iteration) throws ParseException {
        Calendar calendarInstance = Calendar.getInstance();
        calendarInstance.setTime((new SimpleDateFormat("dd/MM/yyyy")).parse(this.params.get("start_date")));

        if (method.equalsIgnoreCase("daily")) {
            calendarInstance.add(Calendar.DAY_OF_MONTH, iteration);
        } else if (method.equalsIgnoreCase("weekly")) {
            calendarInstance.add(Calendar.WEEK_OF_MONTH, iteration);
        } else if (method.equalsIgnoreCase("monthly")) {
            calendarInstance.add(Calendar.MONTH, iteration);
        }

        Event event = new SingleEvent();
        event.setStartDate(new Date(calendarInstance.getTimeInMillis()));
        event.setTimeBlock(Byte.parseByte(this.params.get("time_block")));
        return event;
    }

    Event getEvent() {
        return event;
    }


}
