package com.tasc.microservices.services.schedule.model;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@Table(name = "event")
public abstract class Event {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    public abstract Long getId();

    public abstract void setId(Long id);

    public abstract Byte getTimeBlock();

    public abstract void setTimeBlock(Byte timeBlock);

    public abstract Date getStartDate();

    public abstract void setStartDate(Date date);

    public abstract List<Event> getChildEvents();

    public abstract void setChildEvents(List<Event> childEvents);
}
