package com.tasc.microservices.services.schedule.model;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;

@Entity
public class SingleEvent extends Event {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private Byte timeBlock;
    private Date startDate;

    public SingleEvent() {
    }

    public SingleEvent(Byte timeBlock, Date startDate) {
        this.timeBlock = timeBlock;
        this.startDate = startDate;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Byte getTimeBlock() {
        return timeBlock;
    }

    public void setTimeBlock(Byte timeBlock) {
        this.timeBlock = timeBlock;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public List<Event> getChildEvents() {
        return null;
    }

    public void setChildEvents(List<Event> childEvents) {}
}
