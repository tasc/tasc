package com.tasc.microservices.services.schedule.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

@Entity
public class RepeatingEvent extends Event {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @OneToMany
    private List<Event> childEvents;

    public RepeatingEvent() {
        this.childEvents = new ArrayList<>();
    }

    public RepeatingEvent(List<Event> childEvents) {
        this.childEvents = childEvents;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public List<Event> getChildEvents() {
        return childEvents;
    }

    public void setChildEvents(List<Event> childEvents) {
        this.childEvents = childEvents;
    }

    @Override
    public Byte getTimeBlock() {
        return childEvents.get(0).getTimeBlock();
    }

    @Override
    public void setTimeBlock(Byte timeBlock) {
        childEvents.forEach(event -> event.setTimeBlock(timeBlock));
    }

    @Override
    public Date getStartDate() {
        return null;
    }

    @Override
    public void setStartDate(Date date) {
    }
}
