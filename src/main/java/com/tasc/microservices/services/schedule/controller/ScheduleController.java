package com.tasc.microservices.services.schedule.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.tasc.microservices.services.gateway.config.JwtConfig;
import com.tasc.microservices.services.schedule.model.CalendarScheduleSerializer;
import com.tasc.microservices.common.exceptions.ScheduleNotFoundException;
import com.tasc.microservices.services.schedule.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import com.tasc.microservices.services.accounts.model.AccountRepository;;

import javax.validation.Valid;
import java.text.ParseException;
import java.util.Map;
import java.util.concurrent.ExecutionException;

@RestController
@RequestMapping("/schedule")
public class ScheduleController {

    @Autowired
    private JwtConfig jwtConfig;

    protected ScheduleRepository scheduleRepository;
    protected EventRepository eventRepository;
    protected AccountRepository accountRepository;
    protected EventBuilder eventBuilder;

    @Autowired
    public ScheduleController(
            ScheduleRepository scheduleRepository,
            EventRepository eventRepository,
            EventBuilder eventBuilder,
            AccountRepository accountRepository) {
        this.scheduleRepository = scheduleRepository;
        this.eventRepository = eventRepository;
        this.accountRepository = accountRepository;
        this.eventBuilder = eventBuilder;
    }


    @GetMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
    public Page<Schedule> getSchedule(Pageable pageable) {
        return scheduleRepository.findAll(pageable);
    }

    @GetMapping(value = "/{schedule_id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Schedule getSchedule(@PathVariable(value = "schedule_id") Long scheduleId) {
        return scheduleRepository.findById(scheduleId).orElseThrow(ScheduleNotFoundException::new);
    }

    @GetMapping(value = "/course/calendar", produces = MediaType.APPLICATION_JSON_VALUE)
    public String getScheduleForCalendar(@RequestParam("ta_id") Long taId)
            throws JsonProcessingException {
        Schedule schedule = scheduleRepository.findOneByUserId(taId);
        ObjectMapper mapper = new ObjectMapper();
        SimpleModule module = new SimpleModule();
        String taName = accountRepository.findById(schedule.getUserId()).get().getName();
        module.addSerializer(Schedule.class, new CalendarScheduleSerializer(taName));
        mapper.registerModule(module);
        return mapper.writeValueAsString(schedule);
    }

    @GetMapping(value = "/calendar", produces = MediaType.APPLICATION_JSON_VALUE)
    public String getOwnSchedule(@CookieValue("Authorization") String jwtToken)
            throws JsonProcessingException {
        Long useId = jwtConfig.getId(jwtToken);
        Schedule schedule = scheduleRepository.findOneByUserId(useId);
        ObjectMapper mapper = new ObjectMapper();
        SimpleModule module = new SimpleModule();
        String taName = accountRepository.findById(schedule.getUserId()).get().getName();
        module.addSerializer(Schedule.class, new CalendarScheduleSerializer(taName));
        mapper.registerModule(module);
        return mapper.writeValueAsString(schedule);
    }

    @PostMapping(value = "/create", produces = MediaType.APPLICATION_JSON_VALUE)
    public Schedule postCreateSchedule(@RequestParam("userId") Long userId) {
        Schedule schedule = new Schedule();
        schedule.setUserId(userId);
        return scheduleRepository.save(schedule);
    }

    @PostMapping("/{schedule_id}/addevent")
    public Schedule postAddEvent(
            @PathVariable(value = "schedule_id") Long scheduleId,
            @Valid @RequestBody Map<String, String> body) throws ParseException, ExecutionException, InterruptedException {
        eventBuilder.setParams(body);
        eventBuilder.build();
        eventRepository.save(eventBuilder.getEvent());

        return scheduleRepository.findById(scheduleId).map(schedule -> {
            schedule.addTimeSlot(eventBuilder.getEvent());
            return scheduleRepository.save(schedule);
        }).orElseThrow(ScheduleNotFoundException::new);
    }
}