package com.tasc.microservices.services.schedule.model;

import org.springframework.web.servlet.view.xml.MappingJackson2XmlView;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

@Entity
@Table(name = "course")
public class Course {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String courseName;
    private String courseCode;

    @ElementCollection
    private List<Long> teachingAssistants;

    public Course() {
        this.teachingAssistants = new ArrayList<>();
    }

    public Course(String courseName, String courseCode) {
        this.courseName = courseName;
        this.courseCode = courseCode;
        this.teachingAssistants = new ArrayList<>();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public String getCourseCode() {
        return courseCode;
    }

    public void setCourseCode(String courseCode) {
        this.courseCode = courseCode;
    }

    public List<Long> getTeachingAssistants() {
        return teachingAssistants;
    }

    public void setTeachingAssistants(List<Long> teachingAssistants) {
        this.teachingAssistants = teachingAssistants;
    }

    public void addTeachingAssistants(Long teachingAssistant) {
        this.teachingAssistants.add(teachingAssistant);
    }
}
