package com.tasc.microservices.services.registry;

import com.tasc.microservices.common.annotations.Generated;
import com.tasc.microservices.services.registry.config.WebSecurityConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;
import org.springframework.context.annotation.Import;

@Generated
@SpringBootApplication
@EnableEurekaServer
@Import(WebSecurityConfig.class)
public class RegistryServer {
    public static void main(String[] args) {
        System.out.println("Registry Server running!");
        System.setProperty("spring.config.name", "registry-server");
        SpringApplication.run(RegistryServer.class, args);
    }
}
