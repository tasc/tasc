package com.tasc.microservices.services;

import com.tasc.microservices.common.annotations.Generated;
import com.tasc.microservices.services.accounts.AccountsServer;
import com.tasc.microservices.services.appointment.AppointmentServer;
import com.tasc.microservices.services.gateway.GatewayServer;
import com.tasc.microservices.services.registry.RegistryServer;
import com.tasc.microservices.services.schedule.ScheduleServer;
import com.tasc.microservices.services.web.WebServer;

@Generated
public class Main {
    public static void main(String[] args) {
        String targetServer;
        switch (args.length) {
            case 1:
                targetServer = args[0];
                break;
            default:
                usage();
                return;
        }
        if (targetServer.equalsIgnoreCase("registry")) {
            RegistryServer.main(args);
        } else if (targetServer.equalsIgnoreCase("gateway")) {
            GatewayServer.main(args);
        } else if (targetServer.equalsIgnoreCase("accounts")) {
            AccountsServer.main(args);
        } else if (targetServer.equalsIgnoreCase("schedule")) {
            ScheduleServer.main(args);
        } else if (targetServer.equalsIgnoreCase("web")) {
            WebServer.main(args);
        } else if (targetServer.equalsIgnoreCase("appointment")) {
            AppointmentServer.main(args);
        } else {
            System.out.printf("Unknown service : %s\n", targetServer);
            usage();
        }
    }

    private static void usage() {
        System.out.println("Usage: java -jar JAR_DIR <service-name>");
    }
}
