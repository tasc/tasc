DROP TABLE IF EXISTS appointment;
DROP TABLE IF EXISTS accounts;
DROP TABLE IF EXISTS course;
DROP TABLE IF EXISTS schedule_event;
DROP TABLE IF EXISTS event_event;
DROP TABLE IF EXISTS event;
DROP TABLE IF EXISTS schedule;
DROP SEQUENCE IF EXISTS HIBERNATE_SEQUENCE;
CREATE SEQUENCE HIBERNATE_SEQUENCE;
CREATE TABLE accounts(id BIGINT GENERATED BY DEFAULT AS SEQUENCE HIBERNATE_SEQUENCE PRIMARY KEY, name VARCHAR(255), username VARCHAR(255), email VARCHAR(255), password VARCHAR(255), role VARCHAR(255));
CREATE TABLE course(id BIGINT GENERATED BY DEFAULT AS SEQUENCE HIBERNATE_SEQUENCE PRIMARY KEY, coursename VARCHAR(255), coursecode VARCHAR(255));
CREATE TABLE schedule(id BIGINT GENERATED BY DEFAULT AS SEQUENCE HIBERNATE_SEQUENCE PRIMARY KEY, userId BIGINT);
CREATE TABLE event(id BIGINT GENERATED BY DEFAULT AS SEQUENCE HIBERNATE_SEQUENCE PRIMARY KEY, dtype VARCHAR(31), startdate DATE, timeblock SMALLINT);
CREATE TABLE event_event(childevents_id BIGINT GENERATED BY DEFAULT AS SEQUENCE HIBERNATE_SEQUENCE PRIMARY KEY REFERENCES event (id), repeatingevent_id BIGINT REFERENCES event (id));
CREATE TABLE schedule_event(timeslots_id BIGINT GENERATED BY DEFAULT AS SEQUENCE HIBERNATE_SEQUENCE PRIMARY KEY REFERENCES event (id), schedule_id BIGINT REFERENCES schedule (id));
CREATE TABLE appointment(id BIGINT GENERATED BY DEFAULT AS SEQUENCE HIBERNATE_SEQUENCE PRIMARY KEY, eventId BIGINT, studentId BIGINT, assistantId BIGINT, courseId BIGINT, description VARCHAR(255));