package com.tasc.microservices.common.config;

import com.tasc.microservices.services.accounts.model.Account;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;

import java.util.Collection;

public class MockAccount extends Account {
    public MockAccount(Long id, String name, String roles) {
        super(name, "", "", "", roles);
        this.setId(id);
    }
}
