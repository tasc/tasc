package com.tasc.microservices.services.schedule;


import com.netflix.discovery.converters.Auto;
import com.tasc.microservices.common.config.DBConfiguration;
import com.tasc.microservices.services.schedule.controller.CourseController;
import com.tasc.microservices.services.schedule.controller.EventBuilder;
import com.tasc.microservices.services.schedule.controller.ScheduleController;
import com.tasc.microservices.services.schedule.model.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@RunWith(SpringRunner.class)
@ComponentScan
@EnableAutoConfiguration
@EntityScan("com.tasc.microservices")
@Import(DBConfiguration.class)
@SpringBootTest
@TestPropertySource(properties={
        "eureka.client.enabled=false",
        "spring.application.name=ScheduleServiceControllerTest",
        "spring.jmx.default-domain=ScheduleServiceControllerTest"})
public class ScheduleServiceEntityTest {

    @Autowired
    private CourseRepository courseRepository;

    @Autowired
    private ScheduleRepository scheduleRepository;

    @Autowired
    private EventRepository eventRepository;

    private Course course;
    private Schedule schedule;
    private Event singleEvent;
    private Event repeatingEvent;

    @Before
    public void setUp() throws ParseException {
        course = new Course("Potato Programming", "CSPP01");
        schedule = new Schedule(new ArrayList<>());
        singleEvent = new SingleEvent((byte) 5, new Date((new SimpleDateFormat("dd/MM/yyyy")).parse("20/04/2019").getTime()));
        repeatingEvent = new RepeatingEvent(new ArrayList<>());
        eventRepository.save(singleEvent);  // save this first as it will be used for some other tests

    }

    @Test
    public void courseEntity() {
        Course savedCourse = courseRepository.save(course);

        savedCourse.setId(savedCourse.getId());
        assertThat(savedCourse.getId()).isNotNull();
        assertThat(savedCourse.getCourseName()).isEqualTo("Potato Programming");
        assertThat(savedCourse.getCourseCode()).isEqualTo("CSPP01");
        assertThat(savedCourse.getTeachingAssistants().size()).isEqualTo(0);

        savedCourse.setCourseName("Tomato Programming");
        assertThat(savedCourse.getCourseName()).isEqualTo("Tomato Programming");

        savedCourse.setCourseCode("CSTP01");
        assertThat(savedCourse.getCourseCode()).isEqualTo("CSTP01");

        savedCourse.addTeachingAssistants((long) 10);
        assertThat(savedCourse.getTeachingAssistants().size()).isEqualTo(1);

        savedCourse.setTeachingAssistants(new ArrayList<>());
        assertThat(savedCourse.getTeachingAssistants().size()).isEqualTo(0);
    }

    @Test
    public void scheduleEntity() {
        schedule.setUserId((long) 1);
        schedule.addTimeSlot(singleEvent);
        Schedule savedSchedule = scheduleRepository.save(schedule);

        savedSchedule.setId(savedSchedule.getId());
        assertThat(savedSchedule.getId()).isNotNull();
        assertThat(savedSchedule.getUserId()).isEqualTo((long) 1);
        assertThat(savedSchedule.getTimeSlots()).isNotNull();
    }

    @Test
    public void singleEventEntity() {
        Event savedSingleEvent = eventRepository.save(singleEvent);

        savedSingleEvent.setId(savedSingleEvent.getId());
        assertThat(savedSingleEvent.getId()).isNotNull();
        assertThat(savedSingleEvent.getStartDate()).isNotNull();
        assertThat(savedSingleEvent.getTimeBlock()).isEqualTo((byte) 5);
        assertThat(savedSingleEvent.getChildEvents()).isNull();

        savedSingleEvent.setChildEvents(new ArrayList<>());
        assertThat(savedSingleEvent.getChildEvents()).isNull();
    }

    @Test
    public void repeatingEventEntity() throws ParseException {
        List<Event> childEvents = new ArrayList<>();
        childEvents.add(eventRepository.save(new SingleEvent((byte) 5, new Date((new SimpleDateFormat("dd/MM/yyyy")).parse("20/04/2019").getTime()))));
        repeatingEvent.setChildEvents(childEvents);
        Event savedRepeatingEvent = eventRepository.save(repeatingEvent);

        savedRepeatingEvent.setId(savedRepeatingEvent.getId());
        assertThat(savedRepeatingEvent.getId()).isNotNull();
        assertThat(savedRepeatingEvent.getStartDate()).isNull();
        assertThat(savedRepeatingEvent.getTimeBlock()).isEqualTo((byte) 5);
        assertThat(savedRepeatingEvent.getChildEvents()).isNotNull();

        savedRepeatingEvent.setTimeBlock((byte) 6);
        assertThat(savedRepeatingEvent.getTimeBlock()).isEqualTo((byte) 6);

        savedRepeatingEvent.setStartDate(new Date(0));
        assertThat(savedRepeatingEvent.getStartDate()).isNull();

    }
}
