package com.tasc.microservices.services.schedule;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.tasc.microservices.common.config.DBConfiguration;
import com.tasc.microservices.services.schedule.controller.CourseController;
import com.tasc.microservices.services.schedule.controller.ScheduleController;
import com.tasc.microservices.services.schedule.model.Course;
import com.tasc.microservices.services.schedule.model.Schedule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import javax.transaction.Transactional;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@ComponentScan
@EnableAutoConfiguration
@EntityScan("com.tasc.microservices")
@Import(DBConfiguration.class)
@WebMvcTest({CourseController.class, ScheduleController.class, ScheduleServer.class})
@TestPropertySource(properties={
        "eureka.client.enabled=false",
        "spring.application.name=ScheduleServiceControllerTest",
        "spring.jmx.default-domain=ScheduleServiceControllerTest"})
public class ScheduleServiceControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void contextLoads() {
    }

    @Test
    @Transactional
    public void showSchedule() throws Exception {
        mockMvc.perform(get("/schedule"))
                .andExpect(status().isOk());
    }

    @Test
    @Transactional
    public void postCreateSchedule() throws Exception {
        mockMvc.perform(post("/schedule/create?userId=1"))
                .andExpect(status().isOk());
    }

    @Test
    @Transactional
    public void postAddSingleEvent() throws Exception {
        MvcResult result = mockMvc.perform(post("/schedule/create?userId=2"))
                .andDo(print()).andReturn();
        Schedule schedule = (new ObjectMapper())
                .readValue(result.getResponse().getContentAsString(), Schedule.class);
        String jsonBody = "{\"repeating\":\"false\",\"start_date\":\"22/04/2019\",\"time_block\":\"5\"}";
        mockMvc.perform(post("/schedule/{id}/addevent", schedule.getId())
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(jsonBody))
                .andExpect(status().isOk());
    }

    @Test
    @Transactional
    public void postAddDailyRepeatingEvent() throws Exception {
        MvcResult result = mockMvc.perform(post("/schedule/create?userId=3"))
                .andDo(print()).andReturn();
        Schedule schedule = (new ObjectMapper())
                .readValue(result.getResponse().getContentAsString(), Schedule.class);
        String jsonBody = "{\"repeating\":\"true\",\"times\": 5,\"start_date\":\"22/04/2019\",\"time_block\":\"5\",\"method\":\"daily\"}";
        mockMvc.perform(post("/schedule/{id}/addevent", schedule.getId())
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(jsonBody))
                .andExpect(status().isOk());
    }

    @Test
    @Transactional
    public void postAddWeeklyRepeatingEvent() throws Exception {
        MvcResult result = mockMvc.perform(post("/schedule/create?userId=4"))
                .andDo(print()).andReturn();
        Schedule schedule = (new ObjectMapper())
                .readValue(result.getResponse().getContentAsString(), Schedule.class);
        String jsonBody = "{\"repeating\":\"true\",\"times\": 5,\"start_date\":\"22/04/2019\",\"time_block\":\"5\",\"method\":\"weekly\"}";
        mockMvc.perform(post("/schedule/{id}/addevent", schedule.getId())
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(jsonBody))
                .andExpect(status().isOk());
    }

    @Test
    @Transactional
    public void postAddMonthlyRepeatingEvent() throws Exception {
        MvcResult result = mockMvc.perform(post("/schedule/create?userId=5"))
                .andDo(print()).andReturn();
        Schedule schedule = (new ObjectMapper())
                .readValue(result.getResponse().getContentAsString(), Schedule.class);
        String jsonBody = "{\"repeating\":\"true\",\"times\": 5,\"start_date\":\"22/04/2019\",\"time_block\":\"5\",\"method\":\"monthly\"}";
        mockMvc.perform(post("/schedule/{id}/addevent", schedule.getId())
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(jsonBody))
                .andExpect(status().isOk());
    }

    @Test
    @Transactional
    public void postAddEventNoSchedule() throws Exception {
        String jsonBody = "{\"repeating\":\"false\",\"start_date\":\"22/04/2019\",\"time_block\":\"5\"}";
        mockMvc.perform(post("/schedule/{id}/addevent", 100)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(jsonBody))
                .andExpect(status().isConflict());
    }

    @Test
    @Transactional
    public void getCourse() throws Exception {
        mockMvc.perform(get("/course"))
                .andExpect(status().isOk());
    }

    @Test
    @Transactional
    public void postCreateCourseOkResponse() throws Exception {
        Course aCourse = new Course();
        aCourse.setCourseName("Potato Programming");
        aCourse.setCourseCode("CSPP01");
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String requestJson = ow.writeValueAsString(aCourse);

        mockMvc.perform(post("/course/create")
            .contentType(MediaType.APPLICATION_JSON_UTF8)
            .content(requestJson))
            .andExpect(status().isOk());
    }

    @Test
    @Transactional
    public void postAssignTeachingAssistantToCourse() throws Exception {
        Course aCourse = new Course();
        aCourse.setCourseName("Eggplant Programming");
        aCourse.setCourseCode("CSEP01");
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String requestJson = ow.writeValueAsString(aCourse);

        MvcResult result = mockMvc.perform(post("/course/create")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(requestJson)).andDo(print()).andReturn();

        Course course = (new ObjectMapper())
                .readValue(result.getResponse().getContentAsString(), Course.class);

        mockMvc.perform(post("/course/{course_id}/addassistant?assistant_id={assistant_id}", course.getId(), 10))
                .andExpect(status().isOk());
    }

    @Test
    @Transactional
    public void postAssignTeachingAssistantNoCourse() throws Exception {
        mockMvc.perform(post("/course/{course_id}/addassistant?assistant_id={assistant_id}", 100, 10))
                .andExpect(status().isConflict());
    }

    @Test
    @Transactional
    public void getSchedule() throws Exception {
        MvcResult result = mockMvc.perform(post("/schedule/create?userId=6"))
                .andDo(print()).andReturn();
        Schedule schedule = (new ObjectMapper())
                .readValue(result.getResponse().getContentAsString(), Schedule.class);

        mockMvc.perform(get(String.format("/schedule/%d", schedule.getId())))
                .andExpect(status().isOk());
    }
}
