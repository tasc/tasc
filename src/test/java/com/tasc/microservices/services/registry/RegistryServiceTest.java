package com.tasc.microservices.services.registry;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@TestPropertySource(properties={
        "eureka.client.enabled=false",
        "spring.application.name=RegistryServiceTest",
        "spring.jmx.default-domain=RegistryServiceTest"})
public class RegistryServiceTest {

    @Test
    public void contextLoads() {
    }
}
