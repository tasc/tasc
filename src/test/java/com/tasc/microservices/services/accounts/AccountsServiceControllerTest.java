package com.tasc.microservices.services.accounts;

import com.tasc.microservices.common.config.DBConfiguration;
import com.tasc.microservices.services.accounts.controller.AccountsController;
import com.tasc.microservices.services.accounts.model.Account;
import com.tasc.microservices.services.accounts.model.AccountRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.cloud.netflix.ribbon.RibbonAutoConfiguration;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.cloud.openfeign.FeignAutoConfiguration;
import org.springframework.cloud.openfeign.ribbon.FeignRibbonClientAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import javax.transaction.Transactional;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ImportAutoConfiguration({RibbonAutoConfiguration.class, FeignRibbonClientAutoConfiguration.class, FeignAutoConfiguration.class})
@EnableFeignClients("com.tasc.microservices.common.proxy")
@RunWith(SpringRunner.class)
@ComponentScan
@EnableAutoConfiguration
@EntityScan("com.tasc.microservices")
@Import(DBConfiguration.class)
@WebMvcTest({AccountsController.class})
@TestPropertySource(properties={
        "eureka.client.enabled=false",
        "spring.application.name=AccountsServiceControllerTest",
        "spring.jmx.default-domain=AccountsServiceControllerTest",
        "feign.hystrix.enabled=true"})
public class AccountsServiceControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private AccountRepository accountRepository;

    @Test
    public void contextLoads() {
    }

    @Test
    @Transactional
    public void getAccountsOkResponse() throws Exception {
        mockMvc.perform(get("/accounts"))
                .andExpect(status().isOk());
    }

    @Test
    @Transactional
    public void postCreateAccountsOkResponse() throws Exception {
        mockMvc.perform(post("/register").contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
                .param("name", "Danny August Ramaputra")
                .param("username", "danny")
                .param("email", "danny@tasc.com")
                .param("password", "12345678")
                .param("role", "TA"))
                .andExpect(status().is3xxRedirection());
    }

    @Test
    @Transactional
    public void postEnrollAccountToCourse() throws Exception {
        Account account = new Account();
        account.setName("Nardiena Althafia Pratama");
        account.setUsername("nard");
        account.setEmail("nard@tasc.com");
        account.setPassword("12345678");
        account.setRole("STUDENT");
        account = accountRepository.save(account);

        mockMvc.perform(post("/accounts/{account_id}/enrol?course_id={course_id}", account.getId(), 50))
                .andExpect(status().isOk());
    }

    @Test
    @Transactional
    public void postEnrollAccountNoId() throws Exception {
        mockMvc.perform(post("/accounts/{account_id}/enrol?course_id={course_id}", 90, 50))
                .andExpect(status().isConflict());
    }
}
