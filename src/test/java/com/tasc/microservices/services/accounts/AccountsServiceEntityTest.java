package com.tasc.microservices.services.accounts;

import com.tasc.microservices.common.config.DBConfiguration;
import com.tasc.microservices.services.accounts.model.Account;
import com.tasc.microservices.services.accounts.model.AccountRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@RunWith(SpringRunner.class)
@ComponentScan
@EnableAutoConfiguration
@EntityScan("com.tasc.microservices")
@Import(DBConfiguration.class)
@SpringBootTest
@TestPropertySource(properties={
        "eureka.client.enabled=false",
        "spring.application.name=AccountsServiceEntityTest",
        "spring.jmx.default-domain=AccountsServiceEntityTest"})
public class AccountsServiceEntityTest {

    @Autowired
    private AccountRepository accountRepository;

    private Account account;

    @Before
    public void setUp() {
        account = new Account("Nicolaus CG", "nico", "nico@tasc.com", "12345678", "ADMIN");

    }

    @Test
    public void accountEntity() {
        Account savedAccount = accountRepository.save(account);

        savedAccount.setId(savedAccount.getId());
        assertThat(savedAccount.getId()).isNotNull();
        assertThat(savedAccount.getName()).isEqualTo("Nicolaus CG");
        assertThat(savedAccount.getUsername()).isEqualTo("nico");
        assertThat(savedAccount.getEmail()).isEqualTo("nico@tasc.com");
        assertThat(savedAccount.getPassword()).isEqualTo("12345678");
        assertThat(savedAccount.getRole()).isEqualTo("ADMIN");
        assertThat(savedAccount.getCourse().size()).isEqualTo(0);

        savedAccount.addCourse((long) 50);
        assertThat(savedAccount.getCourse().size()).isEqualTo(1);
        assertThat(savedAccount.getCourse().get(0)).isEqualTo(50);

        savedAccount.setCourse(new ArrayList<>());
        assertThat(savedAccount.getCourse().size()).isEqualTo(0);
    }
}
