package com.tasc.microservices.services.appointment;

import com.tasc.microservices.common.config.DBConfiguration;
import com.tasc.microservices.common.config.MailConfiguration;
import com.tasc.microservices.common.config.MockAccount;
import com.tasc.microservices.services.appointment.controller.AppointmentController;
import com.tasc.microservices.services.gateway.config.JwtConfig;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import javax.servlet.http.Cookie;
import javax.transaction.Transactional;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@ComponentScan
@EnableAutoConfiguration
@EntityScan("com.tasc.microservices")
@Import({DBConfiguration.class, MailConfiguration.class})
@WebMvcTest({AppointmentController.class, AppointmentServer.class})
@TestPropertySource(properties = {
        "eureka.client.enabled=false",
        "spring.application.name=AppointmentServiceControllerTest",
        "spring.jmx.default-domain=AppointmentServiceControllerTest"})
public class AppointmentServiceControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private JwtConfig jwtConfig;

    private Cookie adminCookie;
    private Cookie taCookie;
    private Cookie studentCookie;

    @Before
    public void setUp() {
        this.adminCookie = new Cookie(
                jwtConfig.getHeader(),
                jwtConfig.createToken(new MockAccount((long) 0,"Nicolaus", "ADMIN")));
        this.taCookie = new Cookie(
                jwtConfig.getHeader(),
                jwtConfig.createToken(new MockAccount((long) 1,"Danny", "TA")));
        this.studentCookie = new Cookie(
                jwtConfig.getHeader(),
                jwtConfig.createToken(new MockAccount((long) 2, "Nardiena", "STUDENT")));
    }

    @Test
    public void contextLoads() {
    }

    @Test
    @Transactional
    public void getAppointmentAsStudent() throws Exception {
        mockMvc.perform(get("/appointment")
                .cookie(studentCookie)
                .param("courseId", "10"))
                .andExpect(status().isOk());
    }

    @Test
    @Transactional
    public void getAppointmentAsTa() throws Exception {
        mockMvc.perform(get("/appointment")
                .cookie(taCookie)
                .param("courseId", "10"))
                .andExpect(status().isOk());
    }

    @Test
    @Transactional
    public void getAppointmentAsAdmin() throws Exception {
        mockMvc.perform(get("/appointment")
                .cookie(adminCookie)
                .param("courseId", "10"))
                .andExpect(status().isConflict());
    }

//    @Test
//    @Transactional
//    public void postAddAppointment() throws Exception {
//        String jsonBody = "{\"eventId\": 3, \"assistantId\": 15, \"courseId\": 154, \"description\": \"Exam discussion\"}";
//        mockMvc.perform(post("/appointment/add").cookie(studentCookie)
//                .contentType(MediaType.APPLICATION_JSON_UTF8)
//                .content(jsonBody))
//                .andExpect(status().isInternalServerError());
//    }
}
