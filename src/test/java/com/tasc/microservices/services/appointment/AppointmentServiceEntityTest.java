package com.tasc.microservices.services.appointment;

import com.sun.jersey.core.impl.provider.entity.XMLRootObjectProvider;
import com.tasc.microservices.common.config.DBConfiguration;
import com.tasc.microservices.common.config.MailConfiguration;
import com.tasc.microservices.services.appointment.model.Appointment;
import com.tasc.microservices.services.appointment.model.AppointmentRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@RunWith(SpringRunner.class)
@ComponentScan
@EnableAutoConfiguration
@EntityScan("com.tasc.microservices")
@Import({DBConfiguration.class, MailConfiguration.class})
@SpringBootTest
@TestPropertySource(properties = {
        "eureka.client.enabled=false",
        "spring.application.name=AppointmentServiceEntityTest",
        "spring.jmx.default-domain=AppointmentServiceEntityTest"})
public class AppointmentServiceEntityTest {

    @Autowired
    private AppointmentRepository appointmentRepository;

    private Appointment appointment;

    @Before
    public void setUp() {
        this.appointment = new Appointment((long) 5, (long) 6, (long) 7, (long) 8, "Discuss Intelligent Systems project.");
    }

    @Test
    public void appointmentEntity() {
        Appointment savedAppointment = appointmentRepository.save(appointment);

        savedAppointment.setId(savedAppointment.getId());
        assertThat(savedAppointment.getId()).isNotNull();
        assertThat(savedAppointment.getEventId()).isEqualTo((long) 5);
        assertThat(savedAppointment.getStudentId()).isEqualTo((long) 6);
        assertThat(savedAppointment.getAssistantId()).isEqualTo((long) 7);
        assertThat(savedAppointment.getCourseId()).isEqualTo((long) 8);
        assertThat(savedAppointment.getDescription()).isEqualTo("Discuss Intelligent Systems project.");

        savedAppointment.setEventId((long) 15);
        savedAppointment.setStudentId((long) 16);
        savedAppointment.setAssistantId((long) 17);
        savedAppointment.setCourseId((long) 18);
        savedAppointment.setDescription("Prepare for finals.");
        savedAppointment = appointmentRepository.save(appointment);
        assertThat(savedAppointment.getEventId()).isEqualTo((long) 15);
        assertThat(savedAppointment.getStudentId()).isEqualTo((long) 16);
        assertThat(savedAppointment.getAssistantId()).isEqualTo((long) 17);
        assertThat(savedAppointment.getCourseId()).isEqualTo((long) 18);
        assertThat(savedAppointment.getDescription()).isEqualTo("Prepare for finals.");
    }
}
