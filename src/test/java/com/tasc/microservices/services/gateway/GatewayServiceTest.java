package com.tasc.microservices.services.gateway;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@TestPropertySource(properties={
        "eureka.client.enabled=false",
        "spring.application.name=GatewayServiceTest",
        "spring.jmx.default-domain=GatewayServiceTest"})
public class GatewayServiceTest {

    @Test
    public void contextLoads() {
    }
}
