package com.tasc.microservices.services.web;

import com.tasc.microservices.common.config.MockAccount;
import com.tasc.microservices.services.gateway.config.JwtConfig;
import com.tasc.microservices.services.web.controller.WebController;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.cloud.netflix.ribbon.RibbonAutoConfiguration;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.cloud.openfeign.FeignAutoConfiguration;
import org.springframework.cloud.openfeign.ribbon.FeignRibbonClientAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import javax.servlet.http.Cookie;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ImportAutoConfiguration({RibbonAutoConfiguration.class, FeignRibbonClientAutoConfiguration.class})
@EnableFeignClients("com.tasc.microservices.common.proxy")
@RunWith(SpringRunner.class)
@ComponentScan
@EnableAutoConfiguration
@EntityScan("com.tasc.microservices")
@WebMvcTest({WebController.class})
@TestPropertySource(properties = {
        "eureka.client.enabled=false",
        "spring.application.name=WebServiceControllerTest",
        "spring.jmx.default-domain=WebServiceControllerTest",
        "feign.hystrix.enabled=true"})
class WebServiceControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private JwtConfig jwtConfig;

    private Cookie adminCookie;
    private Cookie taCookie;
    private Cookie studentCookie;

    @Before
    public void setUp() {
        this.adminCookie = new Cookie(
                jwtConfig.getHeader(),
                jwtConfig.createToken(new MockAccount((long) 0,"Nicolaus", "ADMIN")));
        this.taCookie = new Cookie(
                jwtConfig.getHeader(),
                jwtConfig.createToken(new MockAccount((long) 1,"Danny", "TA")));
        this.studentCookie = new Cookie(
                jwtConfig.getHeader(),
                jwtConfig.createToken(new MockAccount((long) 2,"Nardiena", "STUDENT")));
    }

    @Test
    public void contextLoads() {
    }

    @Test
    public void indexAdminPageReturnsOkResponse() throws Exception {
        mockMvc.perform(get("/")
                .cookie(this.adminCookie))
                .andExpect(status().isOk());
    }

    @Test
    public void indexTaPageReturnsOkResponse() throws Exception {
        mockMvc.perform(get("/")
                .cookie(this.taCookie))
                .andExpect(status().isOk());
    }

    @Test
    public void indexStudentPageReturnsOkResponse() throws Exception {
        mockMvc.perform(get("/")
                .cookie(this.studentCookie))
                .andExpect(status().isOk());
    }

    @Test
    public void indexBasePageReturnsOkResponse() throws Exception {
        mockMvc.perform(get("/"))
                .andExpect(status().isOk());
    }

    @Test
    public void taProfilePageReturnsOkResponse() throws Exception {
        mockMvc.perform(get("/taprofile"))
                .andExpect(status().isOk());
    }

    @Test
    public void loginPageReturnsOkResponse() throws Exception {
        mockMvc.perform(get("/login"))
                .andExpect(status().isOk());
    }

    @Test
    public void registerPageReturnsOkResponse() throws Exception {
        mockMvc.perform(get("/register"))
                .andExpect(status().isOk());
    }
}
