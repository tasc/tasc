[![pipeline status](https://gitlab.com/tasc/tasc/badges/staging/pipeline.svg)](https://gitlab.com/tasc/tasc/commits/staging) [![coverage report](https://gitlab.com/tasc/tasc/badges/staging/coverage.svg)](https://gitlab.com/tasc/tasc/commits/staging)

# TASC - Teaching Assistant-Student Connector

## What is it?
*TASC* is an application to help students find assistantship by displaying free schedules of all teaching assistants for each course.

Student can choose a time slot, see details of assistant and make an appointment for an assistantship.

## Development
### Micro-services
Currently, there are 6 micro-services:
- Zuul Gateway Service (`gateway-service` at port `8080`)
- Eureka Discovery Service (`registry-service` at port `1111`)
- Accounts Service (`accounts-service` at port `2222`)
- Schedule Service (`schedule-service` at port `3333`)
- Appointment Service (`appointment-service` at port `5555`)
- Web Service (`web-service` at port `4444`)

### Executing
Use the compiled `jar` to initialize every services. The `registry` service has to be run *first* before the others.

On your terminal, execute `java -jar *.jar SERVICE_NAME`